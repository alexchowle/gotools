package main

import (
	"fmt"
	"net"
	"net/http"
	"os"
	"time"

	"alexchowle/gotools/retry"
)

type noRetryError struct {
	error
}

func (nre noRetryError) Retry() bool { return false }

func main() {
	addFunc := func() (interface{}, error) {
		res := add(1, 2)
		return res, nil
	}

	addResult, err := retry.ExecuteFunc(addFunc)
	dumpReturn(addResult, err)

	////////
	////////

	retry.MinWait = 1000
	retry.MaxWait = 2000
	retry.MaxRetries = 1
	divResult, err := retry.ExecuteFunc(func() (interface{}, error) {
		return divide(2, 0)
	})
	dumpReturn(divResult, err)

	////////
	////////

	getFunc := func() (interface{}, error) {
		c := http.Client{
			Timeout: time.Duration(1 * time.Nanosecond),
		}
		resp, err := c.Get("http://some-flaky-endpoint")

		switch e := err.(type) {
		case nil:
			return resp, nil

		case net.Error:
			if e.Timeout() {
				return nil, err // we will retry on timeout
			}
			return nil, noRetryError{error: err} // we don't want to retry

		default:
			return nil, err
		}

	}

	// create a custom retriable command
	getCmd := retry.Command{
		Function:   getFunc,
		MaxRetries: 2,
		MinWait:    time.Millisecond * 100,
		MaxWait:    time.Millisecond * 750,
		Outputter:  os.Stderr,
	}

	getResult, err := retry.Execute(getCmd)
	dumpReturn(getResult, err)

}

func dumpReturn(result interface{}, err error) {
	if err != nil {
		fmt.Printf("%v\n", err)
		return
	}

	switch v := result.(type) {
	case int:
		fmt.Printf("int result. val: %d\n", v)
	case float64:
		fmt.Printf("float64 result. val: %f\n", v)
	default:
		fmt.Printf("unhandled result. val: %+v\n", v)
	}
}

func add(a, b int) int {
	return a + b
}

func divide(a, b float64) (float64, error) {
	if b == 0 {
		return 0, fmt.Errorf("cannot divide %f by zero", a)
	}
	return a / b, nil
}
