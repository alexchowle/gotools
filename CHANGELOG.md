# CHANGELOG

## 1.3.0 (2019-05-09)

* Support for optional retry based on returned `error`.
* Removed unnecessary cloning of `Command` value.

## 1.2.0 (2019-05-04)

### Added package-level variables to provide more flexibility on `ExecuteFunc` calls

## 1.1.0 (2019-05-01)

### Added validation and tests around invalid Command configurations

## 1.0.0 (2019-04-29)

### Initial release
