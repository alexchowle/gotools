package retry

import (
	"fmt"
	"testing"
	"time"
)

type stubIOWriter struct {
	CallCount int
}

func (s *stubIOWriter) Write(p []byte) (n int, err error) {
	s.CallCount++
	return len(p), nil
}

func TestExecute(t *testing.T) {
	type args struct {
		command Command
	}
	tests := []struct {
		name     string
		args     args
		want     interface{}
		hasError bool
	}{
		{name: "successful command executes once only", args: args{command: newSuccessCommand()}, want: 1},
		{name: "failed command executes twice when MaxRetries is 1", args: args{command: newRetryOnceCommand()}, want: 2, hasError: true},
		{name: "failed command executes five times when MaxRetries is 4", args: args{command: newRetryFourTimesCommand()}, want: 5, hasError: true},
		{name: "failed command does not retry when MaxRetries is 0", args: args{command: newNoRetryOnErrorCommand()}, want: 1, hasError: true},
		{name: "failed command with a negative MinWait will still retry", args: args{command: newNegativeMinWaitCommand()}, want: 2, hasError: true},
		{name: "command with MaxWait lower than MinWait returns error", args: args{command: newMaxWaitLessThanMinWaitCommand()}, want: nil, hasError: true},
		{name: "command with no Function returns error", args: args{command: newFunctionlessCommand()}, want: nil, hasError: true},
		{name: "failed command with a negative MaxRetries does not retry", args: args{command: newNegativeMaxRetriesCommand()}, want: 1, hasError: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Execute(tt.args.command)
			if got != tt.want || (err != nil) != tt.hasError {
				t.Errorf("Execute() = %v (err=%v), want %v (hasError=%v)", got, err, tt.want, tt.hasError)
			}
		})
	}

	// testing that the IOWriter is written to
	sw := stubIOWriter{}
	command := Command{
		MaxRetries: 1,
		MaxWait:    time.Duration(1 * time.Millisecond),
		Function:   newErrorFunc(),
		Outputter:  &sw,
	}

	t.Run("failed Command writes error after final retry", func(t *testing.T) {
		Execute(command)
		if sw.CallCount != 1 {
			t.Error("Command has not written to provided writer")
		}
	})
}

func TestExecuteFunc(t *testing.T) {
	type args struct {
		f func() (interface{}, error)
	}
	tests := []struct {
		name string
		args args
		want interface{}
	}{
		{name: "successful command executes once only", args: args{f: newSuccessFunc()}, want: 1},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got, _ := ExecuteFunc(tt.args.f); got != tt.want {
				t.Errorf("ExecuteFunc() = %v, want %v", got, tt.want)
			}
		})
	}

	// testing the package-level variables
	MaxRetries = 4
	t.Run("package-level MaxRetries change", func(t *testing.T) {
		MaxRetries = 1
		calls, _ := ExecuteFunc(newErrorFunc())
		if calls != 2 {
			t.Error("package-level MaxRetries not acknowledged")
		}
	})
	MaxRetries = 4 // restore default

	// testing that retry is not performed based on error impl.
	t.Run("no retry when specific error returned", func(t *testing.T) {
		calls, _ := ExecuteFunc(newNoRetryErrorFunc())
		if c, ok := calls.(int); !ok {
			t.Errorf("internal test suite error - newNoRetryErrorFunc() did not return an int")
		} else {
			if c != 1 {
				t.Errorf("should not have retried, but retried %d times", c-1)
			}
		}
	})
}

func newSuccessCommand() Command {
	c := defaultCommand()
	c.Function = newSuccessFunc()
	return c
}

func newRetryOnceCommand() Command {
	c := defaultCommand()
	c.Function = newErrorFunc()
	c.MaxRetries = 1
	return c
}

func newNoRetryOnErrorCommand() Command {
	c := defaultCommand()
	c.MaxRetries = 0
	c.Function = newErrorFunc()
	return c
}

func newRetryFourTimesCommand() Command {
	c := defaultCommand()
	c.Function = newErrorFunc()
	c.MaxRetries = 4
	return c
}

func newNegativeMinWaitCommand() Command {
	c := defaultCommand()
	c.MaxRetries = 1
	c.Function = newErrorFunc()
	c.MinWait = time.Duration(-1 * time.Millisecond)
	return c
}

func newMaxWaitLessThanMinWaitCommand() Command {
	c := defaultCommand()
	c.Function = newErrorFunc()
	c.MaxWait = c.MinWait - 1
	return c
}

func newFunctionlessCommand() Command {
	return defaultCommand()
}

func newNegativeMaxRetriesCommand() Command {
	c := defaultCommand()
	c.MaxRetries = -1
	c.Function = newErrorFunc()
	return c
}

func newSuccessFunc() func() (interface{}, error) {
	calls := 0
	return func() (interface{}, error) {
		calls++
		return calls, nil
	}
}

func newErrorFunc() func() (interface{}, error) {
	calls := 0
	return func() (interface{}, error) {
		calls++
		return calls, fmt.Errorf("failed")
	}
}

type noRetryError struct {
	error
}

func (nre noRetryError) Retry() bool { return false }

func newNoRetryErrorFunc() func() (interface{}, error) {
	calls := 0
	return func() (interface{}, error) {
		calls++
		return calls, noRetryError{error: fmt.Errorf("failed")}
	}
}
