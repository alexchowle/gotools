// Package retry provides a simple function execution retrier that enables an arbitrary function to be
// configured for retry on error. Retries are executed at a bounded random interval to provide jitter.
package retry

import (
	"errors"
	"fmt"
	"io"
	"math/rand"
	"time"
)

var (
	// MaxRetries is the maximum number of retry attempts before giving up.
	MaxRetries = 4

	// MinWait is the minimum duration to wait before attempting a retry.
	MinWait = 250 * time.Millisecond

	// MaxWait is the maximum duration to wait before attempting a retry.
	MaxWait = 500 * time.Millisecond
)

// Command is the requested function to be executed.
type Command struct {
	Function       func() (interface{}, error)
	MaxRetries     int
	MinWait        time.Duration
	MaxWait        time.Duration
	Outputter      io.Writer
	currentRetryNo int
	prng           *rand.Rand
}

func defaultCommand() Command {
	return Command{
		MaxRetries: MaxRetries,
		MinWait:    MinWait,
		MaxWait:    MaxWait,
	}
}

// ExecuteFunc executes the provided function. This will retry 4 times at random intervals between 250ms and 500ms.
func ExecuteFunc(f func() (interface{}, error)) (interface{}, error) {
	c := defaultCommand()
	c.Function = f

	return exec(&c)
}

// Execute performs the requested command.
func Execute(cmd Command) (interface{}, error) {
	if err := validateCommand(&cmd); err != nil {
		return nil, fmt.Errorf("retry: %v", err)
	}

	return exec(&cmd)
}

func validateCommand(cmd *Command) error {
	if cmd.MaxRetries < 0 {
		cmd.MaxRetries = 0
	}

	if cmd.MinWait < 0 {
		cmd.MinWait = time.Duration(0 * time.Millisecond)
	}

	if cmd.MaxWait <= cmd.MinWait {
		return fmt.Errorf("MaxWait [%d] must be higher than MinWait [%d]", cmd.MaxWait, cmd.MinWait)
	}

	if cmd.Function == nil {
		return errors.New("a valid Function must be supplied")
	}

	return nil
}

func exec(cmd *Command) (interface{}, error) {
	result, err := cmd.Function()
	if !retriableError(err) || cmd.currentRetryNo >= cmd.MaxRetries {
		return result, err
	}

	outputError(cmd, err)

	if cmd.prng == nil {
		cmd.prng = newPRNG()
	}
	cmd.currentRetryNo++
	time.Sleep(waitDur(cmd))

	return exec(cmd)
}

type retriable interface {
	Retry() bool
}

func retriableError(e error) bool {
	if e == nil {
		return false
	}

	if re, ok := e.(retriable); ok {
		return re.Retry()
	}

	return true
}

func outputError(cmd *Command, err error) {
	if cmd.Outputter != nil {
		errMsg := fmt.Sprintf("retry: error: %v\n", err) // identify yourself!
		io.WriteString(cmd.Outputter, errMsg)            // ignore any errors - this is best effort, here.
	}
}

func newPRNG() *rand.Rand {
	source := rand.NewSource(int64(time.Now().Nanosecond()))
	return rand.New(source)
}

func waitDur(cmd *Command) time.Duration {
	min := int(cmd.MinWait.Nanoseconds())
	max := int(cmd.MaxWait.Nanoseconds())
	return time.Duration(cmd.prng.Intn(max-min)+min) * time.Nanosecond
}
